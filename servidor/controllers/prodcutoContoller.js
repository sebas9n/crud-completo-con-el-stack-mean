const Producto = require("../models/Producto");

exports.crearProduto = async (req, res) => {
    try{
        let producto = new Producto(req.body);
        await producto.save();
        res.send(producto);
    }catch(error){
        console.log(error);
        res.status(500).send("HUBO UN ERROR DE ESCRITURA DE DATOS")
}}

exports.obtenerProducto = async (req,res) => {
    try{
        let producto = await Producto.find();
        
        res.send(producto);
    }catch(error){
        console.log(error);
        res.status(500).send("HUBO UN ERROR LECTURA DE DATOS")}
}