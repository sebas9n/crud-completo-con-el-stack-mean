const mongoose = require('mongoose');
require('dotenv').config({path: 'variables.env'});

const conectarBD = async () => {
    try {
        await mongoose.connect(process.env.BD_MONGO, {
            
        })
        console.log('BD conectada');
        console.log(process.env.BD_MONGO);
    } catch (error){
        console.log(error);
        process.exit(1); //detenemos la app
    }
}

module.exports = conectarBD;    